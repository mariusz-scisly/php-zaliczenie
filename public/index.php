<?php

include_once "../src/App.php";
include_once "../src/Layout.php";
include_once "../src/Database.php";

use App\App;
use App\Database;

$database = new Database();
$database->createOrdersTable();

$app = new App();
$app->run();
