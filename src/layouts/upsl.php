<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            line-height: 1.6;
            margin: 20px;
        }
        h1 {
            color: #333;
        }
        a {
            margin-right: 10px;
            text-decoration: none;
            color: #007bff;
        }
    </style>
</head>
<body>

<h1>Sklep internetowy Mariusza</h1>
<a href="/?page=homepage">Homepage</a>
<a href="/?page=oferta">Oferta</a>

<div>
    <?php echo $content ?>
</div>

</body>
</html>



