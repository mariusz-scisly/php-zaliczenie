<?php

echo "
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Nasz Sklep Internetowy</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            line-height: 1.6;
            margin: 20px;
        }
        header {
            text-align: center;
            padding: 20px;
            background-color: #f4f4f4;
        }
        main {
            max-width: 800px;
            margin: 0 auto;
            text-align: center;
        }
        h1, h2 {
            color: #333;
        }
        p {
            color: #666;
        }
        #ofertaButton {
            display: inline-block;
            padding: 20px;
            font-size: 18px;
            background-color: #4CAF50;
            color: white;
            text-decoration: none;
            border-radius: 8px;
            margin-top: 20px;
        }
        .list-container {
            text-align: left;
        }
        .list-item {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>

    <header>
        <h1>Witaj w Naszym Sklepie Internetowym!</h1>
        <p>Ciesz się z łatwego i wygodnego zakupu w naszym sklepie internetowym.</p>
    </header>

    <main>
        <section>
            <h2>Dlaczego warto wybrać nasz sklep?</h2>
            <div class='list-container'>
                <div class='list-item'>
                    <strong>Bogaty Wybór Produktów:</strong> Oferujemy szeroką gamę wysokiej jakości produktów.
                </div>
                <div class='list-item'>
                    <strong>Szybka Dostawa:</strong> Gwarantujemy ekspresową dostawę, abyś mógł cieszyć się zakupami jak najszybciej.
                </div>
                <div class='list-item'>
                    <strong>Konkurencyjne Ceny:</strong> Nasze ceny są atrakcyjne, co sprawia, że oszczędzasz podczas zakupów.
                </div>
                <div class='list-item'>
                    <strong>Profesjonalne Doradztwo:</strong> Nasz zespół ekspertów służy profesjonalnym doradztwem, abyś dokonał najlepszego wyboru.
                </div>
            </div>
        </section>

        <!-- Dodaj przycisk oferty -->
        <a href='?page=oferta' id='ofertaButton'>OFERTA</a>
    </main>

</body>
</html>
";
?>



