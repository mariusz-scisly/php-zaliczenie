<?php

namespace App;

// Importuj klasę Database
use App\Database;
use TCPDF;

// Utwórz obiekt klasy Database
$database = new Database();

// Inicjalizuj sumę cen zamówienia
$totalOrderPrice = 0;

// Obsłuż przesłane zamówienie
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Pobierz dane do wysyłki
    $firstName = $_POST['firstName'] ?? '';
    $lastName = $_POST['lastName'] ?? '';
    $street = $_POST['street'] ?? '';
    $houseNumber = $_POST['houseNumber'] ?? '';
    $phoneNumber = $_POST['phoneNumber'] ?? '';

    // Wybierz zamówienia z tabeli "orders"
    $orders = $database->select('orders');

    // Wyświetl dane zamówienia
    echo "<h2>Dane do wysyłki:</h2>";
    echo "<p>Imię: $firstName</p>";
    echo "<p>Nazwisko: $lastName</p>";
    echo "<p>Ulica: $street</p>";
    echo "<p>Numer domu: $houseNumber</p>";
    echo "<p>Numer telefonu: $phoneNumber</p>";

    echo "<h2>Składane zamówienia:</h2>";
    foreach ($orders as $order) {
        $productName = htmlspecialchars($order['product_name']);
        $quantity = htmlspecialchars($order['quantity']);
        $totalPrice = htmlspecialchars($order['total_price']);

        // Dodaj cenę produktu do łącznej sumy
        $totalOrderPrice += $totalPrice;

        echo "<p>{$productName} - {$quantity} szt. - {$totalPrice} zł</p>";
    }

    // Wyświetl łączny koszt zamówienia
    echo "<h2>Łączny koszt zamówienia:</h2>";
    echo "<p>Łączna cena: $totalOrderPrice zł";

    // Dodaj przycisk "Generuj PDF"
    echo '<form action="?page=generate_pdf" method="post">';
    echo '<input type="hidden" name="firstName" value="' . htmlspecialchars($firstName) . '">';
    echo '<input type="hidden" name="lastName" value="' . htmlspecialchars($lastName) . '">';
    echo '<input type="hidden" name="street" value="' . htmlspecialchars($street) . '">';
    echo '<input type="hidden" name="houseNumber" value="' . htmlspecialchars($houseNumber) . '">';
    echo '<input type="hidden" name="phoneNumber" value="' . htmlspecialchars($phoneNumber) . '">';
    echo '<input type="hidden" name="totalOrderPrice" value="' . htmlspecialchars($totalOrderPrice) . '">';
    echo '<button type="submit" name="generatePDF">Generuj PDF</button>';
    echo '</form>';
}

?>








