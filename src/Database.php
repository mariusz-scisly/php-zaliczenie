<?php

namespace App;

class Database
{
    private string $hostname;
    private string $dbname;
    private string $username;
    private string $password;
    private \PDO $conn;

    public function __construct()
    {
        $this->hostname = 'localhost';
        $this->dbname = 'example_database';
        $this->username = 'test';
        $this->password = 'test123';

        try {
            $this->conn = new \PDO(
                "mysql:host=$this->hostname;dbname=$this->dbname",
                $this->username,
                $this->password
            );
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function select(string $table, string $condition = null)
    {
        $sql = "SELECT * FROM $table";
        if ($condition) {
            $sql .= " WHERE $condition";
        }

        try {
            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete(string $table, string $condition)
    {
        $sql = "DELETE FROM " . $table . " WHERE " . $condition;

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function edit(string $table, string $condition, array $params)
    {
        $sql = "UPDATE $table SET";

        $fid = array();
        foreach ($params as $k => $v) {
            $fid[] = " `$k` = '$v'";
        }
        $sql .= implode(",", $fid);
        $sql .= " WHERE $condition";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function addShippingData($firstName, $lastName, $street, $houseNumber, $phoneNumber)
    {
        $sql = "INSERT INTO shipping_data (first_name, last_name, street, house_number, phone_number)
            VALUES (:firstName, :lastName, :street, :houseNumber, :phoneNumber)";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':firstName', $firstName);
            $stmt->bindParam(':lastName', $lastName);
            $stmt->bindParam(':street', $street);
            $stmt->bindParam(':houseNumber', $houseNumber);
            $stmt->bindParam(':phoneNumber', $phoneNumber);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function createShippingDataTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS shipping_data (
        id INT PRIMARY KEY AUTO_INCREMENT,
        first_name VARCHAR(255) NOT NULL,
        last_name VARCHAR(255) NOT NULL,
        street VARCHAR(255) NOT NULL,
        house_number VARCHAR(10) NOT NULL,
        phone_number VARCHAR(15) NOT NULL
    )";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function createProductsTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS products (
            id INT PRIMARY KEY AUTO_INCREMENT,
            name VARCHAR(255) NOT NULL,
            price DECIMAL(10, 2) NOT NULL
        )";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function createOrdersTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS orders (
            id INT PRIMARY KEY AUTO_INCREMENT,
            product_name VARCHAR(255) NOT NULL,
            quantity INT NOT NULL,
            total_price DECIMAL(10, 2) NOT NULL
        )";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function addSampleProducts()
    {
        // Dodaj przykładowe produkty, jeśli tabela jest pusta
        $existingProducts = $this->select('products');
        if (empty($existingProducts)) {
            $this->addProduct('T-shirt', 19.99);
            $this->addProduct('Jeans', 39.99);
            $this->addProduct('Sweater', 29.99);
            $this->addProduct('Shorts', 24.99);
            $this->addProduct('Jacket', 59.99);
            $this->addProduct('Dress', 49.99);
            $this->addProduct('Shoes', 69.99);
            $this->addProduct('Hat', 14.99);
            $this->addProduct('Socks', 9.99);
            $this->addProduct('Scarf', 19.99);
            $this->addProduct('Gloves', 12.99);
            $this->addProduct('Pants', 34.99);
            $this->addProduct('Hoodie', 44.99);
            $this->addProduct('Skirt', 29.99);
        }
    }

    public function addProduct($name, $price): void
    {
        $sql = "INSERT INTO products (name, price) VALUES (:name, :price)";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':price', $price);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function addOrder($productName, $productPrice, $quantity): void
    {
        $totalPrice = $productPrice * $quantity;

        $sql = "INSERT INTO orders (product_name, quantity, total_price) VALUES (:productName, :quantity, :totalPrice)";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':productName', $productName);
            $stmt->bindParam(':quantity', $quantity);
            $stmt->bindParam(':totalPrice', $totalPrice);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}
